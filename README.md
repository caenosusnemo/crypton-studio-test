# Crypton Studio test

SPA приложение со списком персонажей Star Wars. Тестовое задание для Crypton Studio

## Используемые технологии

1. Webpack 5
2. Vue 3
3. Vuex 4
4. Vue-router 4
5. SCSS
6. axios
7. lodash

## Установка

Предварительно установить git и npl v16

1. В директории для проекта выполнить:\
   `git clone https://gitlab.com/caenosusnemo/crypton-studio-test.git`
2. Перейти в директрою проекта:\
   `cd crypton-studio-test/`
3. Установить необходимы Node.js модули:\
   `npm i`
4. После этого можно запусить DevServer командой:\
   `npm run dev`\
   Или билд:\
   `npm run build`