/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ 1247:
/***/ ((__unused_webpack_module, __unused_webpack___webpack_exports__, __webpack_require__) => {


// EXTERNAL MODULE: ./node_modules/core-js/stable/index.js
var stable = __webpack_require__(8393);
// EXTERNAL MODULE: ./node_modules/regenerator-runtime/runtime.js
var runtime = __webpack_require__(55);
// EXTERNAL MODULE: ./node_modules/vue/dist/vue.esm-bundler.js + 6 modules
var vue_esm_bundler = __webpack_require__(3597);
// EXTERNAL MODULE: ./node_modules/vue-router/dist/vue-router.esm-bundler.js
var vue_router_esm_bundler = __webpack_require__(4515);
// EXTERNAL MODULE: ./node_modules/vuex/dist/vuex.esm-bundler.js + 5 modules
var vuex_esm_bundler = __webpack_require__(1400);
;// CONCATENATED MODULE: ./node_modules/babel-loader/lib/index.js??clonedRuleSet-1!./node_modules/vue-loader/dist/index.js??ruleSet[1].rules[9].use[0]!./resources/js/components/PagesParts/Card.vue?vue&type=script&setup=true&lang=js


var _withScopeId = function _withScopeId(n) {
  return (0,vue_esm_bundler/* pushScopeId */.dD)("data-v-7a43aa74"), n = n(), (0,vue_esm_bundler/* popScopeId */.Cn)(), n;
};

var _hoisted_1 = {
  "class": "card"
};
var _hoisted_2 = ["src", "alt"];
var _hoisted_3 = {
  "class": "card__wrapper"
};
var _hoisted_4 = {
  "class": "card__context-wrapper"
};
var _hoisted_5 = {
  "class": "card__name distant-galaxy"
};
var _hoisted_6 = {
  "class": "card__homeworld"
};

var _hoisted_7 = /*#__PURE__*/(0,vue_esm_bundler/* createTextVNode */.Uk)(" From: ");

var _hoisted_8 = {
  "class": "distant-galaxy"
};

var _hoisted_9 = /*#__PURE__*/_withScopeId(function () {
  return /*#__PURE__*/(0,vue_esm_bundler/* createElementVNode */._)("i", {
    "class": "fa-solid fa-heart"
  }, null, -1);
});

var _hoisted_10 = [_hoisted_9];



/* harmony default export */ const Cardvue_type_script_setup_true_lang_js = ({
  props: {
    person: Object,
    isReady: Boolean
  },
  setup: function setup(__props) {
    var props = __props;
    var store = (0,vuex_esm_bundler/* useStore */.oR)();
    var route = (0,vue_router_esm_bundler/* useRoute */.yj)();
    var router = (0,vue_router_esm_bundler/* useRouter */.tv)();
    var filterValue = (0,vue_esm_bundler/* computed */.Fl)(function () {
      return store.state.filterValue;
    });

    var toggleToFavorites = function toggleToFavorites() {
      if (route.name === 'Home') {
        store.dispatch('addToFavorites', props.person);
        router.push({
          name: 'Favorites'
        });
      } else {
        store.dispatch('removeFromFavorites', props.person.id);
      }
    };

    return function (_ctx, _cache) {
      return (0,vue_esm_bundler/* withDirectives */.wy)(((0,vue_esm_bundler/* openBlock */.wg)(), (0,vue_esm_bundler/* createElementBlock */.iD)("div", _hoisted_1, [(0,vue_esm_bundler/* createElementVNode */._)("img", {
        src: "https://starwars-visualguide.com/assets/img/characters/".concat(__props.person.id, ".jpg"),
        alt: __props.person.name,
        "class": "card__img"
      }, null, 8, _hoisted_2), (0,vue_esm_bundler/* createElementVNode */._)("div", _hoisted_3, [(0,vue_esm_bundler/* createElementVNode */._)("div", _hoisted_4, [(0,vue_esm_bundler/* createElementVNode */._)("div", _hoisted_5, (0,vue_esm_bundler/* toDisplayString */.zw)(__props.person.name), 1), (0,vue_esm_bundler/* createElementVNode */._)("div", _hoisted_6, [_hoisted_7, (0,vue_esm_bundler/* createElementVNode */._)("span", _hoisted_8, (0,vue_esm_bundler/* toDisplayString */.zw)(__props.person.homeworld), 1)])]), (0,vue_esm_bundler/* createElementVNode */._)("button", {
        "class": "card__button",
        onClick: toggleToFavorites
      }, _hoisted_10)])], 512)), [[vue_esm_bundler/* vShow */.F8, (0,vue_esm_bundler/* unref */.SU)(filterValue) ? (0,vue_esm_bundler/* unref */.SU)(filterValue) === __props.person.gender : true]]);
    };
  }
});
;// CONCATENATED MODULE: ./resources/js/components/PagesParts/Card.vue?vue&type=script&setup=true&lang=js
 
// EXTERNAL MODULE: ./node_modules/vue-loader/dist/exportHelper.js
var exportHelper = __webpack_require__(9886);
;// CONCATENATED MODULE: ./resources/js/components/PagesParts/Card.vue



;


const __exports__ = /*#__PURE__*/(0,exportHelper/* default */.Z)(Cardvue_type_script_setup_true_lang_js, [['__scopeId',"data-v-7a43aa74"]])

/* harmony default export */ const Card = (__exports__);
;// CONCATENATED MODULE: ./node_modules/babel-loader/lib/index.js??clonedRuleSet-1!./node_modules/vue-loader/dist/index.js??ruleSet[1].rules[9].use[0]!./resources/js/components/PagesParts/Pagination.vue?vue&type=script&setup=true&lang=js


var Paginationvue_type_script_setup_true_lang_js_withScopeId = function _withScopeId(n) {
  return _pushScopeId("data-v-0e31d42c"), n = n(), _popScopeId(), n;
};

var Paginationvue_type_script_setup_true_lang_js_hoisted_1 = {
  key: 0,
  "class": "pagination"
};


/* harmony default export */ const Paginationvue_type_script_setup_true_lang_js = ({
  props: {
    parent: String
  },
  setup: function setup(__props) {
    var props = __props;
    var store = (0,vuex_esm_bundler/* useStore */.oR)();
    var pagesCount = (0,vue_esm_bundler/* computed */.Fl)(function () {
      var count = props.parent === 'Home' ? store.state.count : store.state.favorites.length;
      return count > 10 ? Math.ceil(count / store.state.perPage) : 1;
    });
    return function (_ctx, _cache) {
      var _component_router_link = (0,vue_esm_bundler/* resolveComponent */.up)("router-link");

      return (0,vue_esm_bundler/* unref */.SU)(pagesCount) > 1 ? ((0,vue_esm_bundler/* openBlock */.wg)(), (0,vue_esm_bundler/* createElementBlock */.iD)("div", Paginationvue_type_script_setup_true_lang_js_hoisted_1, [((0,vue_esm_bundler/* openBlock */.wg)(true), (0,vue_esm_bundler/* createElementBlock */.iD)(vue_esm_bundler/* Fragment */.HY, null, (0,vue_esm_bundler/* renderList */.Ko)((0,vue_esm_bundler/* unref */.SU)(pagesCount), function (page) {
        return (0,vue_esm_bundler/* openBlock */.wg)(), (0,vue_esm_bundler/* createBlock */.j4)(_component_router_link, {
          to: {
            name: __props.parent,
            params: {
              page: page
            }
          }
        }, {
          "default": (0,vue_esm_bundler/* withCtx */.w5)(function () {
            return [(0,vue_esm_bundler/* createTextVNode */.Uk)((0,vue_esm_bundler/* toDisplayString */.zw)(page), 1)];
          }),
          _: 2
        }, 1032, ["to"]);
      }), 256))])) : (0,vue_esm_bundler/* createCommentVNode */.kq)("", true);
    };
  }
});
;// CONCATENATED MODULE: ./resources/js/components/PagesParts/Pagination.vue?vue&type=script&setup=true&lang=js
 
;// CONCATENATED MODULE: ./resources/js/components/PagesParts/Pagination.vue



;


const Pagination_exports_ = /*#__PURE__*/(0,exportHelper/* default */.Z)(Paginationvue_type_script_setup_true_lang_js, [['__scopeId',"data-v-0e31d42c"]])

/* harmony default export */ const Pagination = (Pagination_exports_);
// EXTERNAL MODULE: ./node_modules/lodash/lodash.js
var lodash = __webpack_require__(3383);
var lodash_default = /*#__PURE__*/__webpack_require__.n(lodash);
;// CONCATENATED MODULE: ./node_modules/babel-loader/lib/index.js??clonedRuleSet-1!./node_modules/vue-loader/dist/index.js??ruleSet[1].rules[9].use[0]!./resources/js/pages/Favorites.vue?vue&type=script&setup=true&lang=js

var Favoritesvue_type_script_setup_true_lang_js_hoisted_1 = {
  "class": "page"
};

var Favoritesvue_type_script_setup_true_lang_js_hoisted_2 = /*#__PURE__*/(0,vue_esm_bundler/* createElementVNode */._)("h1", null, "Favorites", -1);

var Favoritesvue_type_script_setup_true_lang_js_hoisted_3 = {
  key: 1,
  "class": "page__message"
};






/* harmony default export */ const Favoritesvue_type_script_setup_true_lang_js = ({
  setup: function setup(__props) {
    var store = (0,vuex_esm_bundler/* useStore */.oR)();
    var route = (0,vue_router_esm_bundler/* useRoute */.yj)();
    var list = (0,vue_esm_bundler/* computed */.Fl)(function () {
      return store.getters.getFavorites(route.params.page || 1);
    });
    return function (_ctx, _cache) {
      return (0,vue_esm_bundler/* openBlock */.wg)(), (0,vue_esm_bundler/* createElementBlock */.iD)("div", Favoritesvue_type_script_setup_true_lang_js_hoisted_1, [Favoritesvue_type_script_setup_true_lang_js_hoisted_2, !(0,vue_esm_bundler/* unref */.SU)((lodash_default())).isEmpty((0,vue_esm_bundler/* unref */.SU)(list)) ? ((0,vue_esm_bundler/* openBlock */.wg)(), (0,vue_esm_bundler/* createBlock */.j4)(vue_esm_bundler/* TransitionGroup */.W3, {
        key: 0,
        tag: "div",
        "class": "page__list"
      }, {
        "default": (0,vue_esm_bundler/* withCtx */.w5)(function () {
          return [((0,vue_esm_bundler/* openBlock */.wg)(true), (0,vue_esm_bundler/* createElementBlock */.iD)(vue_esm_bundler/* Fragment */.HY, null, (0,vue_esm_bundler/* renderList */.Ko)((0,vue_esm_bundler/* unref */.SU)(list), function (person) {
            return (0,vue_esm_bundler/* openBlock */.wg)(), (0,vue_esm_bundler/* createBlock */.j4)((0,vue_esm_bundler/* unref */.SU)(Card), {
              "class": "page__card",
              person: person,
              key: "favorite-person_".concat(person.id)
            }, null, 8, ["person"]);
          }), 128))];
        }),
        _: 1
      })) : ((0,vue_esm_bundler/* openBlock */.wg)(), (0,vue_esm_bundler/* createElementBlock */.iD)("div", Favoritesvue_type_script_setup_true_lang_js_hoisted_3, "Favorites list is empty")), (0,vue_esm_bundler/* createVNode */.Wm)((0,vue_esm_bundler/* unref */.SU)(Pagination), {
        parent: 'Favorites',
        key: "pagination-favorites"
      })]);
    };
  }
});
;// CONCATENATED MODULE: ./resources/js/pages/Favorites.vue?vue&type=script&setup=true&lang=js
 
;// CONCATENATED MODULE: ./resources/js/pages/Favorites.vue



const Favorites_exports_ = Favoritesvue_type_script_setup_true_lang_js;

/* harmony default export */ const Favorites = (Favorites_exports_);
;// CONCATENATED MODULE: ./node_modules/babel-loader/lib/index.js??clonedRuleSet-1!./node_modules/vue-loader/dist/index.js??ruleSet[1].rules[9].use[0]!./resources/js/pages/Home.vue?vue&type=script&setup=true&lang=js


var Homevue_type_script_setup_true_lang_js_hoisted_1 = /*#__PURE__*/(0,vue_esm_bundler/* createElementVNode */._)("h1", null, "Home", -1);

var Homevue_type_script_setup_true_lang_js_hoisted_2 = {
  key: 0,
  "class": "page__list"
};
var Homevue_type_script_setup_true_lang_js_hoisted_3 = {
  key: 1,
  "class": "page__message"
};
var Homevue_type_script_setup_true_lang_js_hoisted_4 = {
  key: 2,
  "class": "page__message"
};






/* harmony default export */ const Homevue_type_script_setup_true_lang_js = ({
  setup: function setup(__props) {
    var store = (0,vuex_esm_bundler/* useStore */.oR)();
    var route = (0,vue_router_esm_bundler/* useRoute */.yj)();
    var count = (0,vue_esm_bundler/* computed */.Fl)(function () {
      return store.state.count;
    });
    var personsList = (0,vue_esm_bundler/* computed */.Fl)(function () {
      return store.state.personsList;
    });
    (0,vue_esm_bundler/* onBeforeMount */.wF)(function () {
      return store.dispatch('getPersons');
    });
    (0,vue_esm_bundler/* watch */.YP)(function () {
      return route.params.page;
    }, function (page) {
      store.dispatch('getPersons', page);
    });
    return function (_ctx, _cache) {
      return (0,vue_esm_bundler/* openBlock */.wg)(), (0,vue_esm_bundler/* createElementBlock */.iD)("div", null, [Homevue_type_script_setup_true_lang_js_hoisted_1, !(0,vue_esm_bundler/* unref */.SU)((lodash_default())).isEmpty((0,vue_esm_bundler/* unref */.SU)(personsList)) ? ((0,vue_esm_bundler/* openBlock */.wg)(), (0,vue_esm_bundler/* createElementBlock */.iD)("div", Homevue_type_script_setup_true_lang_js_hoisted_2, [((0,vue_esm_bundler/* openBlock */.wg)(true), (0,vue_esm_bundler/* createElementBlock */.iD)(vue_esm_bundler/* Fragment */.HY, null, (0,vue_esm_bundler/* renderList */.Ko)((0,vue_esm_bundler/* unref */.SU)(personsList), function (person) {
        return (0,vue_esm_bundler/* openBlock */.wg)(), (0,vue_esm_bundler/* createBlock */.j4)((0,vue_esm_bundler/* unref */.SU)(Card), {
          "class": "page__card",
          person: person,
          key: "person_".concat(person.id)
        }, null, 8, ["person"]);
      }), 128))])) : (0,vue_esm_bundler/* unref */.SU)(count).value === 0 ? ((0,vue_esm_bundler/* openBlock */.wg)(), (0,vue_esm_bundler/* createElementBlock */.iD)("div", Homevue_type_script_setup_true_lang_js_hoisted_3, " Nothing found")) : ((0,vue_esm_bundler/* openBlock */.wg)(), (0,vue_esm_bundler/* createElementBlock */.iD)("div", Homevue_type_script_setup_true_lang_js_hoisted_4, "Fetching...")), (0,vue_esm_bundler/* createVNode */.Wm)((0,vue_esm_bundler/* unref */.SU)(Pagination), {
        parent: "Home",
        key: "pagination-home"
      })]);
    };
  }
});
;// CONCATENATED MODULE: ./resources/js/pages/Home.vue?vue&type=script&setup=true&lang=js
 
;// CONCATENATED MODULE: ./resources/js/pages/Home.vue



const Home_exports_ = Homevue_type_script_setup_true_lang_js;

/* harmony default export */ const Home = (Home_exports_);
;// CONCATENATED MODULE: ./node_modules/babel-loader/lib/index.js??clonedRuleSet-1!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/dist/index.js??ruleSet[1].rules[9].use[0]!./resources/js/pages/NotFound.vue?vue&type=template&id=bb504ba6&scoped=true


var NotFoundvue_type_template_id_bb504ba6_scoped_true_withScopeId = function _withScopeId(n) {
  return _pushScopeId("data-v-bb504ba6"), n = n(), _popScopeId(), n;
};

var NotFoundvue_type_template_id_bb504ba6_scoped_true_hoisted_1 = {
  "class": "not-found"
};
function render(_ctx, _cache, $props, $setup, $data, $options) {
  return (0,vue_esm_bundler/* openBlock */.wg)(), (0,vue_esm_bundler/* createElementBlock */.iD)("div", NotFoundvue_type_template_id_bb504ba6_scoped_true_hoisted_1, "404 Not Found");
}
;// CONCATENATED MODULE: ./resources/js/pages/NotFound.vue?vue&type=template&id=bb504ba6&scoped=true

;// CONCATENATED MODULE: ./node_modules/babel-loader/lib/index.js??clonedRuleSet-1!./node_modules/vue-loader/dist/index.js??ruleSet[1].rules[9].use[0]!./resources/js/pages/NotFound.vue?vue&type=script&lang=js
/* harmony default export */ const NotFoundvue_type_script_lang_js = ({
  name: 'NotFound'
});
;// CONCATENATED MODULE: ./resources/js/pages/NotFound.vue?vue&type=script&lang=js
 
;// CONCATENATED MODULE: ./resources/js/pages/NotFound.vue




;


const NotFound_exports_ = /*#__PURE__*/(0,exportHelper/* default */.Z)(NotFoundvue_type_script_lang_js, [['render',render],['__scopeId',"data-v-bb504ba6"]])

/* harmony default export */ const NotFound = (NotFound_exports_);
;// CONCATENATED MODULE: ./resources/router.js




var routes = [{
  path: '/favorites/:id',
  component: Favorites,
  name: 'Favorites'
}, {
  path: '/favorites/',
  component: Favorites,
  name: 'Favorites'
}, {
  path: '/',
  component: Home,
  name: 'Home'
}, {
  path: '/:id',
  component: Home,
  name: 'Page'
}, {
  path: '/:pathMatch(.*)*',
  name: 'not-found',
  component: NotFound
}];
/* harmony default export */ const router = ((0,vue_router_esm_bundler/* createRouter */.p7)({
  routes: routes,
  history: (0,vue_router_esm_bundler/* createWebHistory */.PO)()
}));
;// CONCATENATED MODULE: ./resources/js/utils/Functions.js

var getId = function getId(url) {
  return lodash_default().toNumber(lodash_default().nth(lodash_default().split(url, '/'), -2));
};
;// CONCATENATED MODULE: ./resources/store/index.js
var _mutations;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }



var favorites;

try {
  favorites = JSON.parse(localStorage.favorites);
} catch (err) {
  console.error(err);
}

/* harmony default export */ const store = (new vuex_esm_bundler/* createStore */.MT({
  state: function state() {
    return {
      favorites: favorites || [],
      favoritesToShow: favorites || [],
      filterValue: '',
      personsList: [],
      count: 1,
      next: '',
      previous: '',
      results: [],
      perPage: 10
    };
  },
  getters: {
    getFavorites: function getFavorites(state) {
      return function (page) {
        var tens = page * 10;
        var favoritesLength = state.favoritesToShow.length;
        var start = favoritesLength > 10 ? tens - 10 : 0;
        return _.slice(state.favoritesToShow, start, tens - 1);
      };
    }
  },
  actions: {
    getPersons: function getPersons(_ref) {
      var _arguments = arguments;
      return _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
        var commit, dispatch, state, page;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                commit = _ref.commit, dispatch = _ref.dispatch, state = _ref.state;
                page = _arguments.length > 1 && _arguments[1] !== undefined ? _arguments[1] : 1;
                commit('SET_PERSONS_LIST', []);
                commit('SET_COUNT', 0);
                axios.get("https://swapi.dev/api/people/?page=".concat(page)).then(function (res) {
                  dispatch('setPersons', res.data);
                  dispatch('getPersonsContext');
                })["catch"](function (error) {
                  return console.error(error);
                });

              case 5:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },
    setPersons: function setPersons(_ref2, persons) {
      var commit = _ref2.commit;
      commit('SET_PERSONS', persons);
    },
    getPersonsContext: function getPersonsContext(_ref3) {
      return _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
        var commit, state, list;
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                commit = _ref3.commit, state = _ref3.state;
                list = [];
                _context2.next = 4;
                return Promise.all(state.results.map(function (person) {
                  return axios.get(person.homeworld);
                })).then(function (res) {
                  list = res;
                });

              case 4:
                list = list.map(function (world) {
                  return world.data.name;
                });
                list = list.map(function (homeworld, i) {
                  return _objectSpread(_objectSpread({}, state.results[i]), {}, {
                    homeworld: homeworld
                  });
                });
                commit('SET_PERSONS_LIST', list);

              case 7:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    },
    addToFavorites: function addToFavorites(_ref4, person) {
      var commit = _ref4.commit,
          state = _ref4.state;

      var favorites = _.union(state.favorites, [person]);

      commit('SET_FAVORITES', favorites);
    },
    removeFromFavorites: function removeFromFavorites(_ref5, id) {
      var commit = _ref5.commit,
          state = _ref5.state;

      var favorites = _.reject(state.favorites, function (person) {
        return person.id === id;
      });

      commit('SET_FAVORITES', favorites);
    },
    searchFavorites: function searchFavorites(_ref6, searchValue) {
      var commit = _ref6.commit,
          state = _ref6.state;
      var filteredFavorites = state.favorites.filter(function (person) {
        return _.includes(person.name.toLowerCase(), searchValue.value.toLowerCase());
      });
      commit('SET_FAVORITES_TO_SHOW', filteredFavorites);
    },
    setFilterValue: function setFilterValue(_ref7, value) {
      var commit = _ref7.commit;
      commit('SET_FILTER_VALUE', value);
    }
  },
  mutations: (_mutations = {}, _defineProperty(_mutations, 'SET_PERSONS', function SET_PERSONS(state, data) {
    state.count = data.count;
    state.next = data.next;
    state.previous = data.previous;
    state.results = data.results.map(function (person) {
      return _objectSpread(_objectSpread({}, person), {}, {
        id: getId(person.url) || person.url
      });
    });

    if (state.results) {
      state.personsList = state.results.map(function (person) {
        return _objectSpread(_objectSpread({}, person), {}, {
          homeworld: ''
        });
      });
    }
  }), _defineProperty(_mutations, 'SET_PERSONS_LIST', function SET_PERSONS_LIST(state, list) {
    state.personsList = list;
  }), _defineProperty(_mutations, 'SET_FAVORITES', function SET_FAVORITES(state, favorites) {
    state.favorites = _.sortBy(favorites, 'id');
    localStorage.favorites = JSON.stringify(favorites);
  }), _defineProperty(_mutations, 'SET_FAVORITES_TO_SHOW', function SET_FAVORITES_TO_SHOW(state, favorites) {
    state.favoritesToShow = favorites;
  }), _defineProperty(_mutations, 'RESET_FAVORITES', function RESET_FAVORITES(state) {
    state.favoritesToShow = state.favorites;
  }), _defineProperty(_mutations, 'SET_FILTER_VALUE', function SET_FILTER_VALUE(state, value) {
    state.filterValue = value;
  }), _defineProperty(_mutations, 'SET_COUNT', function SET_COUNT(state, value) {
    state.count = value;
  }), _mutations)
}));
;// CONCATENATED MODULE: ./node_modules/babel-loader/lib/index.js??clonedRuleSet-1!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/dist/index.js??ruleSet[1].rules[9].use[0]!./resources/App.vue?vue&type=template&id=86b0980e&scoped=true


var Appvue_type_template_id_86b0980e_scoped_true_withScopeId = function _withScopeId(n) {
  return (0,vue_esm_bundler/* pushScopeId */.dD)("data-v-86b0980e"), n = n(), (0,vue_esm_bundler/* popScopeId */.Cn)(), n;
};

var Appvue_type_template_id_86b0980e_scoped_true_hoisted_1 = /*#__PURE__*/Appvue_type_template_id_86b0980e_scoped_true_withScopeId(function () {
  return /*#__PURE__*/(0,vue_esm_bundler/* createElementVNode */._)("footer", null, " Crypton test task © 2022 ", -1);
});

function Appvue_type_template_id_86b0980e_scoped_true_render(_ctx, _cache, $props, $setup, $data, $options) {
  var _component_nav_bar = (0,vue_esm_bundler/* resolveComponent */.up)("nav-bar");

  var _component_router_view = (0,vue_esm_bundler/* resolveComponent */.up)("router-view");

  return (0,vue_esm_bundler/* openBlock */.wg)(), (0,vue_esm_bundler/* createElementBlock */.iD)(vue_esm_bundler/* Fragment */.HY, null, [(0,vue_esm_bundler/* createElementVNode */._)("div", null, [(0,vue_esm_bundler/* createVNode */.Wm)(_component_nav_bar), (0,vue_esm_bundler/* createVNode */.Wm)(_component_router_view, null, {
    "default": (0,vue_esm_bundler/* withCtx */.w5)(function (_ref) {
      var Component = _ref.Component;
      return [(0,vue_esm_bundler/* createVNode */.Wm)(vue_esm_bundler/* Transition */.uT, {
        name: "fade"
      }, {
        "default": (0,vue_esm_bundler/* withCtx */.w5)(function () {
          return [((0,vue_esm_bundler/* openBlock */.wg)(), (0,vue_esm_bundler/* createBlock */.j4)(vue_esm_bundler/* KeepAlive */.Ob, null, [((0,vue_esm_bundler/* openBlock */.wg)(), (0,vue_esm_bundler/* createBlock */.j4)((0,vue_esm_bundler/* resolveDynamicComponent */.LL)(Component)))], 1024))];
        }),
        _: 2
      }, 1024)];
    }),
    _: 1
  })]), Appvue_type_template_id_86b0980e_scoped_true_hoisted_1], 64);
}
;// CONCATENATED MODULE: ./resources/App.vue?vue&type=template&id=86b0980e&scoped=true

;// CONCATENATED MODULE: ./node_modules/babel-loader/lib/index.js??clonedRuleSet-1!./node_modules/vue-loader/dist/index.js??ruleSet[1].rules[9].use[0]!./resources/js/components/NavBarParts/Filtration.vue?vue&type=script&setup=true&lang=js


var Filtrationvue_type_script_setup_true_lang_js_withScopeId = function _withScopeId(n) {
  return (0,vue_esm_bundler/* pushScopeId */.dD)("data-v-018bdc85"), n = n(), (0,vue_esm_bundler/* popScopeId */.Cn)(), n;
};

var Filtrationvue_type_script_setup_true_lang_js_hoisted_1 = {
  "class": "filtration"
};
var Filtrationvue_type_script_setup_true_lang_js_hoisted_2 = {
  "class": "filtration__search"
};
var Filtrationvue_type_script_setup_true_lang_js_hoisted_3 = ["onKeyup"];

var Filtrationvue_type_script_setup_true_lang_js_hoisted_4 = /*#__PURE__*/Filtrationvue_type_script_setup_true_lang_js_withScopeId(function () {
  return /*#__PURE__*/(0,vue_esm_bundler/* createElementVNode */._)("i", {
    "class": "fa-solid fa-magnifying-glass"
  }, null, -1);
});

var Filtrationvue_type_script_setup_true_lang_js_hoisted_5 = [Filtrationvue_type_script_setup_true_lang_js_hoisted_4];
var Filtrationvue_type_script_setup_true_lang_js_hoisted_6 = {
  "class": "filtration__filters"
};

var Filtrationvue_type_script_setup_true_lang_js_hoisted_7 = /*#__PURE__*/Filtrationvue_type_script_setup_true_lang_js_withScopeId(function () {
  return /*#__PURE__*/(0,vue_esm_bundler/* createElementVNode */._)("span", null, "Gender filter:", -1);
});

var Filtrationvue_type_script_setup_true_lang_js_hoisted_8 = {
  "class": "filtration__filters-wrapper"
};

var Filtrationvue_type_script_setup_true_lang_js_hoisted_9 = /*#__PURE__*/(0,vue_esm_bundler/* createTextVNode */.Uk)(" Male ");

var Filtrationvue_type_script_setup_true_lang_js_hoisted_10 = /*#__PURE__*/(0,vue_esm_bundler/* createTextVNode */.Uk)(" Female ");




/* harmony default export */ const Filtrationvue_type_script_setup_true_lang_js = ({
  setup: function setup(__props) {
    var route = (0,vue_router_esm_bundler/* useRoute */.yj)();
    var store = (0,vuex_esm_bundler/* useStore */.oR)();
    var searchValue = (0,vue_esm_bundler/* ref */.iH)('');
    var toShowFavorites = (0,vue_esm_bundler/* ref */.iH)([]);
    var favorites = (0,vue_esm_bundler/* computed */.Fl)(function () {
      return store.state.favorites;
    });

    var searchPeople = function searchPeople() {
      if (route.name === 'Home') {
        axios.get("https://swapi.dev/api/people/?search=".concat(searchValue.value)).then(function (res) {
          store.dispatch('setPersons', res.data);
        });
      } else {
        store.dispatch('searchFavorites', searchValue);
      }
    };

    var filterValue = (0,vue_esm_bundler/* computed */.Fl)({
      get: function get() {
        return store.state.filterValue;
      },
      set: function set(value) {
        return store.dispatch('setFilterValue', value);
      }
    });

    var resetFilters = function resetFilters() {
      if (searchValue.value) {
        route.name === 'Home' ? store.dispatch('getPersons') : store.commit('RESET_FAVORITES');
        searchValue.value = '';
      }

      store.dispatch('setFilterValue', '');
    };

    return function (_ctx, _cache) {
      return (0,vue_esm_bundler/* openBlock */.wg)(), (0,vue_esm_bundler/* createElementBlock */.iD)("div", Filtrationvue_type_script_setup_true_lang_js_hoisted_1, [(0,vue_esm_bundler/* createElementVNode */._)("div", Filtrationvue_type_script_setup_true_lang_js_hoisted_2, [(0,vue_esm_bundler/* withDirectives */.wy)((0,vue_esm_bundler/* createElementVNode */._)("input", {
        type: "text",
        "onUpdate:modelValue": _cache[0] || (_cache[0] = function ($event) {
          return searchValue.value = $event;
        }),
        onKeyup: (0,vue_esm_bundler/* withKeys */.D2)(searchPeople, ["enter"])
      }, null, 40, Filtrationvue_type_script_setup_true_lang_js_hoisted_3), [[vue_esm_bundler/* vModelText */.nr, searchValue.value]]), (0,vue_esm_bundler/* createElementVNode */._)("button", {
        onClick: searchPeople
      }, Filtrationvue_type_script_setup_true_lang_js_hoisted_5)]), (0,vue_esm_bundler/* createElementVNode */._)("div", Filtrationvue_type_script_setup_true_lang_js_hoisted_6, [Filtrationvue_type_script_setup_true_lang_js_hoisted_7, (0,vue_esm_bundler/* createElementVNode */._)("div", Filtrationvue_type_script_setup_true_lang_js_hoisted_8, [(0,vue_esm_bundler/* createElementVNode */._)("label", null, [Filtrationvue_type_script_setup_true_lang_js_hoisted_9, (0,vue_esm_bundler/* withDirectives */.wy)((0,vue_esm_bundler/* createElementVNode */._)("input", {
        type: "radio",
        name: "gender",
        value: "male",
        "onUpdate:modelValue": _cache[1] || (_cache[1] = function ($event) {
          return (0,vue_esm_bundler/* isRef */.dq)(filterValue) ? filterValue.value = $event : null;
        })
      }, null, 512), [[vue_esm_bundler/* vModelRadio */.G2, (0,vue_esm_bundler/* unref */.SU)(filterValue)]])]), (0,vue_esm_bundler/* createElementVNode */._)("label", null, [Filtrationvue_type_script_setup_true_lang_js_hoisted_10, (0,vue_esm_bundler/* withDirectives */.wy)((0,vue_esm_bundler/* createElementVNode */._)("input", {
        type: "radio",
        name: "gender",
        value: "female",
        "onUpdate:modelValue": _cache[2] || (_cache[2] = function ($event) {
          return (0,vue_esm_bundler/* isRef */.dq)(filterValue) ? filterValue.value = $event : null;
        })
      }, null, 512), [[vue_esm_bundler/* vModelRadio */.G2, (0,vue_esm_bundler/* unref */.SU)(filterValue)]])])])]), (0,vue_esm_bundler/* createElementVNode */._)("button", {
        onClick: resetFilters,
        "class": "filtration__reset"
      }, "Reset")]);
    };
  }
});
;// CONCATENATED MODULE: ./resources/js/components/NavBarParts/Filtration.vue?vue&type=script&setup=true&lang=js
 
;// CONCATENATED MODULE: ./resources/js/components/NavBarParts/Filtration.vue



;


const Filtration_exports_ = /*#__PURE__*/(0,exportHelper/* default */.Z)(Filtrationvue_type_script_setup_true_lang_js, [['__scopeId',"data-v-018bdc85"]])

/* harmony default export */ const Filtration = (Filtration_exports_);
;// CONCATENATED MODULE: ./node_modules/babel-loader/lib/index.js??clonedRuleSet-1!./node_modules/vue-loader/dist/index.js??ruleSet[1].rules[9].use[0]!./resources/js/components/NavBar.vue?vue&type=script&setup=true&lang=js


var NavBarvue_type_script_setup_true_lang_js_withScopeId = function _withScopeId(n) {
  return (0,vue_esm_bundler/* pushScopeId */.dD)("data-v-014fe9bf"), n = n(), (0,vue_esm_bundler/* popScopeId */.Cn)(), n;
};

var NavBarvue_type_script_setup_true_lang_js_hoisted_1 = {
  "class": "navbar"
};

var NavBarvue_type_script_setup_true_lang_js_hoisted_2 = /*#__PURE__*/NavBarvue_type_script_setup_true_lang_js_withScopeId(function () {
  return /*#__PURE__*/(0,vue_esm_bundler/* createElementVNode */._)("p", null, "Star Wars", -1);
});

var NavBarvue_type_script_setup_true_lang_js_hoisted_3 = /*#__PURE__*/NavBarvue_type_script_setup_true_lang_js_withScopeId(function () {
  return /*#__PURE__*/(0,vue_esm_bundler/* createElementVNode */._)("p", null, "Heroes", -1);
});

var NavBarvue_type_script_setup_true_lang_js_hoisted_4 = {
  "class": "navbar__wrapper"
};
var NavBarvue_type_script_setup_true_lang_js_hoisted_5 = {
  "class": "navbar__group"
};

var NavBarvue_type_script_setup_true_lang_js_hoisted_6 = /*#__PURE__*/(0,vue_esm_bundler/* createTextVNode */.Uk)("Home");

var NavBarvue_type_script_setup_true_lang_js_hoisted_7 = /*#__PURE__*/(0,vue_esm_bundler/* createTextVNode */.Uk)("Favorites");

var NavBarvue_type_script_setup_true_lang_js_hoisted_8 = ["onClick"];

var NavBarvue_type_script_setup_true_lang_js_hoisted_9 = /*#__PURE__*/NavBarvue_type_script_setup_true_lang_js_withScopeId(function () {
  return /*#__PURE__*/(0,vue_esm_bundler/* createElementVNode */._)("i", {
    "class": "fa-solid fa-bars"
  }, null, -1);
});

var NavBarvue_type_script_setup_true_lang_js_hoisted_10 = [NavBarvue_type_script_setup_true_lang_js_hoisted_9];


/* harmony default export */ const NavBarvue_type_script_setup_true_lang_js = ({
  setup: function setup(__props) {
    var screenWidth = (0,vue_esm_bundler/* ref */.iH)(window.innerWidth);
    var showNavBar = (0,vue_esm_bundler/* ref */.iH)(true);

    var toggleNavBar = function toggleNavBar() {
      showNavBar.value = !showNavBar.value;
    };

    window.addEventListener('resize', function () {
      screenWidth.value = window.innerWidth;

      if (!showNavBar.value) {
        showNavBar.value = screenWidth.value > 950;
      }
    });
    return function (_ctx, _cache) {
      var _component_router_link = (0,vue_esm_bundler/* resolveComponent */.up)("router-link");

      return (0,vue_esm_bundler/* openBlock */.wg)(), (0,vue_esm_bundler/* createElementBlock */.iD)("nav", NavBarvue_type_script_setup_true_lang_js_hoisted_1, [(0,vue_esm_bundler/* createVNode */.Wm)(_component_router_link, {
        to: "/",
        "class": "navbar__logo"
      }, {
        "default": (0,vue_esm_bundler/* withCtx */.w5)(function () {
          return [NavBarvue_type_script_setup_true_lang_js_hoisted_2, NavBarvue_type_script_setup_true_lang_js_hoisted_3];
        }),
        _: 1
      }), (0,vue_esm_bundler/* createElementVNode */._)("div", NavBarvue_type_script_setup_true_lang_js_hoisted_4, [(0,vue_esm_bundler/* createVNode */.Wm)(vue_esm_bundler/* Transition */.uT, {
        name: "fade"
      }, {
        "default": (0,vue_esm_bundler/* withCtx */.w5)(function () {
          return [(0,vue_esm_bundler/* withDirectives */.wy)((0,vue_esm_bundler/* createElementVNode */._)("div", NavBarvue_type_script_setup_true_lang_js_hoisted_5, [(0,vue_esm_bundler/* createVNode */.Wm)(_component_router_link, {
            to: "/",
            "class": "navbar__link"
          }, {
            "default": (0,vue_esm_bundler/* withCtx */.w5)(function () {
              return [NavBarvue_type_script_setup_true_lang_js_hoisted_6];
            }),
            _: 1
          }), (0,vue_esm_bundler/* createVNode */.Wm)(_component_router_link, {
            to: "/favorites",
            "class": "navbar__link"
          }, {
            "default": (0,vue_esm_bundler/* withCtx */.w5)(function () {
              return [NavBarvue_type_script_setup_true_lang_js_hoisted_7];
            }),
            _: 1
          })], 512), [[vue_esm_bundler/* vShow */.F8, showNavBar.value]])];
        }),
        _: 1
      }), (0,vue_esm_bundler/* createVNode */.Wm)(vue_esm_bundler/* Transition */.uT, {
        name: "fade"
      }, {
        "default": (0,vue_esm_bundler/* withCtx */.w5)(function () {
          return [(0,vue_esm_bundler/* withDirectives */.wy)((0,vue_esm_bundler/* createVNode */.Wm)((0,vue_esm_bundler/* unref */.SU)(Filtration), null, null, 512), [[vue_esm_bundler/* vShow */.F8, showNavBar.value]])];
        }),
        _: 1
      }), screenWidth.value < 951 ? ((0,vue_esm_bundler/* openBlock */.wg)(), (0,vue_esm_bundler/* createElementBlock */.iD)("button", {
        key: 0,
        onClick: (0,vue_esm_bundler/* withModifiers */.iM)(toggleNavBar, ["prevent"]),
        "class": "navbar__menu-button"
      }, NavBarvue_type_script_setup_true_lang_js_hoisted_10, 8, NavBarvue_type_script_setup_true_lang_js_hoisted_8)) : (0,vue_esm_bundler/* createCommentVNode */.kq)("", true)])]);
    };
  }
});
;// CONCATENATED MODULE: ./resources/js/components/NavBar.vue?vue&type=script&setup=true&lang=js
 
;// CONCATENATED MODULE: ./resources/js/components/NavBar.vue



;


const NavBar_exports_ = /*#__PURE__*/(0,exportHelper/* default */.Z)(NavBarvue_type_script_setup_true_lang_js, [['__scopeId',"data-v-014fe9bf"]])

/* harmony default export */ const NavBar = (NavBar_exports_);
;// CONCATENATED MODULE: ./node_modules/babel-loader/lib/index.js??clonedRuleSet-1!./node_modules/vue-loader/dist/index.js??ruleSet[1].rules[9].use[0]!./resources/App.vue?vue&type=script&lang=js


/* harmony default export */ const Appvue_type_script_lang_js = ({
  name: 'App',
  components: {
    Home: Home,
    NavBar: NavBar
  }
});
;// CONCATENATED MODULE: ./resources/App.vue?vue&type=script&lang=js
 
;// CONCATENATED MODULE: ./resources/App.vue




;


const App_exports_ = /*#__PURE__*/(0,exportHelper/* default */.Z)(Appvue_type_script_lang_js, [['render',Appvue_type_template_id_86b0980e_scoped_true_render],['__scopeId',"data-v-86b0980e"]])

/* harmony default export */ const App = (App_exports_);
// EXTERNAL MODULE: ./node_modules/axios/index.js
var node_modules_axios = __webpack_require__(4089);
var axios_default = /*#__PURE__*/__webpack_require__.n(node_modules_axios);
// EXTERNAL MODULE: ./node_modules/@fortawesome/fontawesome-free/js/fontawesome.js
var fontawesome = __webpack_require__(9732);
// EXTERNAL MODULE: ./node_modules/@fortawesome/fontawesome-free/js/solid.js
var solid = __webpack_require__(3790);
;// CONCATENATED MODULE: ./resources/index.js










window.axios = (axios_default());

var resources_lodash = __webpack_require__(3383);

window._ = resources_lodash;
var app = (0,vue_esm_bundler/* createApp */.ri)(App);
app.use(router);
app.use(store);
app.mount('.App');
app.provide('_', resources_lodash);

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			id: moduleId,
/******/ 			loaded: false,
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/amd options */
/******/ 	(() => {
/******/ 		__webpack_require__.amdO = {};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	(() => {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = (result, chunkIds, fn, priority) => {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var [chunkIds, fn, priority] = deferred[i];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every((key) => (__webpack_require__.O[key](chunkIds[j])))) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					var r = fn();
/******/ 					if (r !== undefined) result = r;
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	(() => {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = (module) => {
/******/ 			var getter = module && module.__esModule ?
/******/ 				() => (module['default']) :
/******/ 				() => (module);
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/global */
/******/ 	(() => {
/******/ 		__webpack_require__.g = (function() {
/******/ 			if (typeof globalThis === 'object') return globalThis;
/******/ 			try {
/******/ 				return this || new Function('return this')();
/******/ 			} catch (e) {
/******/ 				if (typeof window === 'object') return window;
/******/ 			}
/******/ 		})();
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/node module decorator */
/******/ 	(() => {
/******/ 		__webpack_require__.nmd = (module) => {
/******/ 			module.paths = [];
/******/ 			if (!module.children) module.children = [];
/******/ 			return module;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	(() => {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			179: 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = (chunkId) => (installedChunks[chunkId] === 0);
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = (parentChunkLoadingFunction, data) => {
/******/ 			var [chunkIds, moreModules, runtime] = data;
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			if(chunkIds.some((id) => (installedChunks[id] !== 0))) {
/******/ 				for(moduleId in moreModules) {
/******/ 					if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 						__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 					}
/******/ 				}
/******/ 				if(runtime) var result = runtime(__webpack_require__);
/******/ 			}
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkId] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunkcrypton_studio_test"] = self["webpackChunkcrypton_studio_test"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, [260], () => (__webpack_require__(1247)))
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	
/******/ })()
;
//# sourceMappingURL=maina821a2159e99fa83ea06.js.map