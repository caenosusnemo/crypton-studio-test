import 'core-js/stable';
import 'regenerator-runtime/runtime';
import {createApp} from 'vue';
import router from './router';
import store from './store/index';
import App from './App';
import axios from 'axios';
import '@fortawesome/fontawesome-free/js/fontawesome';
import '@fortawesome/fontawesome-free/js/solid';
import styles from '@scss/styles.scss';

window.axios = axios;
const lodash = require('lodash');
window._ = lodash;
const app = createApp(App);
app.use(router);
app.use(store);
app.mount('.App');
app.provide('_', lodash);