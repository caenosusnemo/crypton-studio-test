import _ from 'lodash';
export const getId = (url) => {
  return _.toNumber(_.nth(_.split(url, '/'), -2));
};