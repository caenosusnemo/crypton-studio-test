import {createRouter, createWebHistory} from 'vue-router';
import Favorites from '@js/pages/Favorites';
import Home from '@js/pages/Home';
import NotFound from '@js/pages/NotFound';

const routes = [
  {path: '/favorites/:id', component: Favorites, name: 'Favorites'},
  {path: '/favorites/', component: Favorites, name: 'Favorites'},
  {path: '/', component: Home, name: 'Home'},
  {path: '/:id', component: Home, name: 'Page'},
  {path: '/:pathMatch(.*)*', name: 'not-found', component: NotFound},
];
export default createRouter({routes, history: createWebHistory()});
