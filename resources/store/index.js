import {createStore} from 'vuex';
import {getId} from '@js/utils/Functions';

let favorites;
try {
  favorites = JSON.parse(localStorage.favorites);
} catch (err) {
  console.error(err);
}
export default new createStore({
  state() {
    return {
      favorites: favorites || [],
      favoritesToShow: favorites || [],
      filterValue: '',
      personsList: [],
      count: 1,
      next: '',
      previous: '',
      results: [],
      perPage: 10,
    };
  },
  getters: {
    getFavorites: state => page => {
      const tens = page * 10;
      const favoritesLength = state.favoritesToShow.length;
      const start = favoritesLength > 10 ? tens - 10 : 0;
      return _.slice(state.favoritesToShow, start, tens - 1);
    },
  },
  actions: {
    async getPersons({commit, dispatch, state}, page = 1) {
      commit('SET_PERSONS_LIST', []);
      commit('SET_COUNT', 0);
      axios.get(`https://swapi.dev/api/people/?page=${page}`).
          then(res => {
            dispatch('setPersons', res.data);
            dispatch('getPersonsContext');
          }).
          catch(error => console.error(error));
    },
    setPersons({commit}, persons) {
      commit('SET_PERSONS', persons);
    },
    async getPersonsContext({commit, state}) {
      let list = [];
      await Promise.all(
          state.results.map(person => axios.get(person.homeworld))).
          then(res => {
            list = res;
          });
      list = list.map(world => world.data.name);
      list = list.map((homeworld, i) =>
          ({...state.results[i], homeworld: homeworld}));
      commit('SET_PERSONS_LIST', list);
    },
    addToFavorites({commit, state}, person) {
      const favorites = _.union(state.favorites, [person]);
      commit('SET_FAVORITES', favorites);
    },
    removeFromFavorites({commit, state}, id) {
      let favorites = _.reject(state.favorites, (person) => person.id === id);
      commit('SET_FAVORITES', favorites);
    },
    searchFavorites({commit, state}, searchValue) {
      let filteredFavorites = state.favorites.filter(
          person => _.includes(person.name.toLowerCase(),
              searchValue.value.toLowerCase()));
      commit('SET_FAVORITES_TO_SHOW', filteredFavorites);
    },
    setFilterValue({commit}, value) {
      commit('SET_FILTER_VALUE', value);
    },
  },
  mutations: {
    ['SET_PERSONS'](state, data) {
      state.count = data.count;
      state.next = data.next;
      state.previous = data.previous;
      state.results = data.results.map(
          person => ({...person, id: getId(person.url) || person.url}));
      if (state.results) {
        state.personsList = state.results.map(
            person => ({...person, homeworld: ''}));
      }

    },
    ['SET_PERSONS_LIST'](state, list) {
      state.personsList = list;
    },
    ['SET_FAVORITES'](state, favorites) {
      state.favorites = _.sortBy(favorites, 'id');
      localStorage.favorites = JSON.stringify(favorites);
    },
    ['SET_FAVORITES_TO_SHOW'](state, favorites) {
      state.favoritesToShow = favorites;
    },
    ['RESET_FAVORITES'](state) {
      state.favoritesToShow = state.favorites;
    },
    ['SET_FILTER_VALUE'](state, value) {
      state.filterValue = value;
    },
    ['SET_COUNT'](state, value) {
      state.count = value;
    },

  },
});