const path = require('path');
const {VueLoaderPlugin} = require('vue-loader');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const webpack = require('webpack');
const mode = process.env.NODE_ENV === 'production' ?
    'production' :
    'development';
const isProd = mode === 'production';
const optimization = () => {
  const config = {
    splitChunks: {
      chunks: 'all',
    },
  };
  if (isProd) {
    config.minimizer = [
      new MiniCssExtractPlugin(),
    ];
    config.minimize = true;
  }
  return config;
};
const plugins = () => {
  return [
    new MiniCssExtractPlugin(),
    new VueLoaderPlugin(),
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, './resources/index.html'),
    }),
    new webpack.DefinePlugin({
      // Drop Options API from bundle
      __VUE_OPTIONS_API__: false,
      __VUE_PROD_DEVTOOLS__: !isProd,
    }),
  ];
};
module.exports = {
  mode: mode,
  entry: path.resolve(__dirname, './resources/index.js'),
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: '[name][contenthash].js',
  },
  devServer: {
    static: {
      directory: path.join(__dirname, 'public'),
    },
    compress: true,
    port: 1024,
    host: '0.0.0.0',
    historyApiFallback: true,
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env'],
        },
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,

          },
          'css-loader',
          'postcss-loader',
        ],
      },
      {
        test: /.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          'sass-loader',
          'postcss-loader',
        ],
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader',
      },
    ],
  },
  resolve: {
    extensions: ['.js', '.vue', '.scss'],
    alias: {
      '@js': path.resolve(__dirname, 'resources/js'),
      '@store': path.resolve(__dirname, 'resources/store'),
      '@scss': path.resolve(__dirname, 'resources/scss'),
      'vue': 'vue/dist/vue.esm-bundler.js',
    },
  },
  plugins: plugins(),
  optimization: optimization(),
  devtool: isProd ? 'nosources-source-map' : 'source-map',
};